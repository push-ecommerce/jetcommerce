﻿
var data = 
[
    {
        "marca": "BMW",
        "modelo": "Z4 coup",
        "ano": "2007",
        "cor": "laranja",
        "placa": "NEJ-6420"
    },
    {
        "marca": "Honda",
        "modelo": "Accord Coupe EX",
        "ano": "1991",
        "cor": "Preto",
        "placa": "NEJ-5085"
    },
    {
        "marca": "Peugeot",
        "modelo": "207 Sedan Passion XR 1.4 Flex 8V 4p",
        "ano": "2009",
        "cor": "Bege",
        "placa": "NES-0659"
    },
    {
        "marca": "Fiat",
        "modelo": "MOBI Like 1.0 Fire Flex 5p.",
        "ano": "2017",
        "cor": "Cinza",
        "placa": "NEP-1151"
    },
    {
        "marca": "Ford",
        "modelo": "Ranger XLS 3.2 20V 4x4 CD Diesel Mec.",
        "ano": "2013",
        "cor": "Verde",
        "placa": "NFA-5128"
    }
]
;
$( document ).ready(function() {
$('#txt-search').keyup(function(){
            var searchField = $(this).val();
            if(searchField === '')  {
                $('#filter-records').html('');
                return;
            }
            
            var regex = new RegExp(searchField, "i");
            var output = '<div class="row">';
            var count = 1;
              $.each(data, function(key, val){
                if (val.placa.search(regex) != -1) {
                  output += '<div class="resultados">';                 
                  output += '<h2 class="placa">' + val.placa + '</h2>';
                  output += '<span class="hidden modelo">' + val.modelo + '</span>';
                  output += '<span class="hidden ano">' + val.ano + '</span>';
                  output += '<span class="hidden cor">' + val.cor + '</span>';
                  output += '<span class="hidden marca">' + val.marca + '</span>';
                  output += '</div>';
                  if(count%2 == 0){
                    output += '</div><div class="row">'
                  }
                  count++;
                }
              });
              output += '</div>';
              $('#filter-records').html(output);

             //if($(".resultados").length == 1){
                $(".resultados").click(function() {   
                    $(this).find(".marca").clone().appendTo(".placas-modal .marca");
                    $(this).find(".modelo").clone().appendTo(".placas-modal .modelo");
                    $(this).find(".ano").clone().appendTo(".placas-modal .ano");
                    $(this).find(".cor").clone().appendTo(".placas-modal .cor");
                    $(this).find(".placa").clone().appendTo(".placas-modal .placa");                         
                    $(".placas-modal").modal('setting', 'closable', false).modal('show');
                });
            //}

             $(".placas-modal .close").click(function() {   
                    $('.placas-modal .marca').text("");
                    $('.placas-modal .modelo').text("");
                    $('.placas-modal .ano').text("");
                    $('.placas-modal .cor').text("");
                    $('.placas-modal .placa').text("");
          
             });

        });
    });
